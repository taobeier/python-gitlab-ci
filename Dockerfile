FROM python:3.8-slim

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

EXPOSE 8888

COPY hello_world.py .
CMD [ "python", "hello_world.py" ]
